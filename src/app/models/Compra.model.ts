import { Producto } from './Producto.model';

export class Compra {
    producto: Producto;
    cantidad: number;
    id?: number;
    nombre?: string;
    subtotal: number;
}
