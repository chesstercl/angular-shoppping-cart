import { Producto } from '../models/Producto.model';

export const PRODUCTOS: Producto[] = [
    {id: 1, nombre: 'Lápiz pasta', precio: 300, cantidadStock: 6},
    {id: 2, nombre: 'Lápiz grafito', precio: 100, cantidadStock: 5},
    {id: 3, nombre: 'Goma borrar', precio: 200, cantidadStock: 8},
];
