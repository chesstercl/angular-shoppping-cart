import { Injectable } from '@angular/core';
import { Compra } from '../models/Compra.model';

@Injectable({
  providedIn: 'root'
})
export class CompraService {

  compraItems: Compra[] = [];

  constructor() { }

  getCompraItems() {
     if (localStorage.getItem('compraItems') === null) {
       this.compraItems = [];
     } else {
       this.compraItems = JSON.parse(localStorage.getItem('compraItems'));
       console.log('PreCompraItems: ', this.compraItems);
     }
     console.log('CompraItems: ', this.compraItems);
     return this.compraItems;
  }

  addCompraItems(compra: Compra) {
    this.compraItems.push({...compra});
    localStorage.setItem('compras', JSON.stringify(this.compraItems));
  }

  deleteCompraItems(compra: Compra) {
    for (let i = 0; i < this.compraItems.length; i++) {
      if (compra == this.compraItems[i]) {
        this.compraItems.splice(i, 1);
        localStorage.setItem('compras', JSON.stringify(this.compraItems));
      }
    }
  }
}
