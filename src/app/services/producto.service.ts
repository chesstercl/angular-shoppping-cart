import { Injectable } from '@angular/core';
import { PRODUCTOS } from '../data/producto.data';
import { Producto } from '../models/Producto.model';

@Injectable({
  providedIn: 'root'
})


export class ProductoService {

  productos: Producto[];

  constructor() {
    localStorage.setItem('productos', JSON.stringify(PRODUCTOS));
   }
/*
   getProductos() {
     this.productos = JSON.parse(localStorage.getItem('productos'));
     console.log(localStorage.getItem('productos'));
     return this.productos;
   }*/
}
