import { Component, OnInit } from '@angular/core';
import { ProductoService } from 'src/app/services/producto.service';
import { PRODUCTOS} from 'src/app/data/producto.data';
import { CompraService } from '../../services/compra.service';
import { Producto } from 'src/app/models/Producto.model';
import { Compra } from 'src/app/models/Compra.model';

@Component({
  selector: 'app-compra-form',
  templateUrl: './compra-form.component.html',
  styleUrls: ['./compra-form.component.css']
})
export class CompraFormComponent implements OnInit {

  productos = PRODUCTOS;
  item: Compra = {
    producto: undefined,
    cantidad: 1,
    subtotal: 0
  };
  compras: Compra[];
/*   total = 0; */

  constructor(public compraServicio: CompraService) { }

  ngOnInit() {
    this.compras = this.compraServicio.compraItems;
    /*this.productos = this.compraServicio.getProductos();*/
  }

  addItemCompra() {
    this.subTotal();
    this.compraServicio.addCompraItems(this.item);
    this.total();
    console.log('TOT_AL:', this.total());
    this.compras = this.compraServicio.compraItems;
  }

  subTotal() {
/*   = 1; */
    this.item.subtotal = (this.item.cantidad) * (this.item.producto.precio);
    return this.item.subtotal;
  }

  total() {
      /* const total = this.compras.reduce((sum, compras) => {
        return sum + this.item.subtotal; }, 0);
      return total; */
    return this.compras.reduce((suma, item) => suma + (item.producto.precio * item.cantidad), 0);
  }

}

