import {
  Component,
  OnInit
} from '@angular/core';
import {
  CompraService
} from '../../services/compra.service';
import {
  Compra
} from 'src/app/models/Compra.model';

@Component({
  selector: 'app-compra-list',
  templateUrl: './compra-list.component.html',
  styleUrls: ['./compra-list.component.css']
})
export class CompraListComponent implements OnInit {
  /*item: any[];*/

  constructor(public compraService: CompraService) {}
  item: Compra = {
    producto: null,
    cantidad: 1,
    subtotal: 0
  };
  compras = [];
  // total: number;


  ngOnInit() {
    this.compras = this.compraService.compraItems;
    /* console.log(this.total);
    this.total = this.compras.reduce((sum, item) => {
        return sum + this.item.subtotal; }, 0);
    console.log('Total: ', this.total); */
    console.log('item: ', this.item);
    console.log('compras: ', this.compras);
  }

  addCantidadItem() {

  }

  deleteCantidadItem() {

  }

  removeItem(item: Compra) {
    console.log('item: ', item);
    for (let i = 0; i < this.compras.length; i++) {
      if (item == this.compras[i]) {
        this.compras.splice(i, 1);
        localStorage.setItem('compras', JSON.stringify(this.compras));
        console.log('compras: ', this.compras);
      }
    }
  }
  total() {
    return this.compras.reduce((suma, item) => suma + (item.producto.precio * item.cantidad), 0);
  }

  /*   total() {
      const total = this.compras.reduce((sum, compras) => {
        return sum + this.item.subtotal; }, 0);
      return total;
    } */
}
