import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { CompraListComponent } from './components/compra-list/compra-list.component';
import { CompraFormComponent } from './components/compra-form/compra-form.component';
import { CompraService} from './services/compra.service';
import { ProductoService } from './services/producto.service';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    CompraListComponent,
    CompraFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    FontAwesomeModule
  ],
  providers: [ProductoService, CompraService],
  bootstrap: [AppComponent]
})
export class AppModule { }
